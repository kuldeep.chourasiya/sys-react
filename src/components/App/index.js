import React from 'react';

import Shelf from '../Shelf';
import Test from '../Test';
import Filter from '../Shelf/Filter';
import GithubCorner from '../github/Corner';
import FloatCart from '../FloatCart';

const App = () => (
  <React.Fragment>
    <GithubCorner />
    <main>
      
      <Filter />
      <Shelf />
    </main>
    <FloatCart />
  </React.Fragment>
);

export default App;
